﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Unions
{
    public abstract class Union
    {
        private static Dictionary<Type, Func<Type, IEnumerable<Type>>> TypeProviders { get; } =
            new Dictionary<Type, Func<Type, IEnumerable<Type>>>();

        static Union()
        {
            TypeProviders.Add(typeof(Union<,>), FindForGenericUnion);
        }

        public static Type[] GetAcceptableTypes(Type type)
        {
            if(!IsUnionType(type))
                throw new ArgumentException($"Type {type.Name} is not a union");
            if(typeof(Union) == type)
                throw new ArgumentException($"Type Union is a abstract union class");

            var provider = FindProvider(type);

            if(provider == null)
                throw new ArgumentOutOfRangeException($"Type {type.Name} is unknown by union type system");

            return provider(type).ToArray();
        }

        public bool IsEmpty => ContentType == null;

        public abstract Type ContentType { get; }

        public abstract object Content { get; set; }

        public static bool IsUnionType(Type type)
        {
            return typeof(Union).GetTypeInfo().IsAssignableFrom(type.GetTypeInfo());
        }

        private static Func<Type, IEnumerable<Type>> FindProvider(Type type)
        {
            Func<Type, IEnumerable<Type>> provider = null;
            if (!TypeProviders.TryGetValue(type, out provider))
            {
                if (IsUnionType(type.GetTypeInfo().BaseType))
                    provider = FindProvider(type.GetTypeInfo().BaseType);
                if (provider == null)
                {
                    if (type.IsConstructedGenericType)
                    {
                        type = type.GetGenericTypeDefinition();
                        provider = FindProvider(type);
                    }
                }
            }
            return provider;
        }

        private static IEnumerable<Type> FindForGenericUnion(Type inType)
        {
            var first = FindGenericUnion(inType);
            var types = new List<Type>();
            var pair = first.GenericTypeArguments;
            foreach(var type in pair)
            {
                var provider = FindProvider(type);
                if(provider != null)
                    types.AddRange(provider(type));
                else
                    types.Add(type);
            }
            return types.Distinct();
        }

        private static Type FindGenericUnion(Type type)
        {
            while (!type.IsConstructedGenericType && type.GetGenericTypeDefinition() != typeof (Union<,>))
                type = type.GetTypeInfo().BaseType;
            return type;
        }
    }

    public class Union<T1, T2> : Union
    {
        private bool? _left = null;

        private T1 _t1 = default(T1);
        private T2 _t2 = default(T2);

        public Union()
        {
        }

        public Union(T1 value)
        {
            _left = true;
            _t1 = value;
        }

        public Union(T2 value)
        {
            _left = false;
            _t2 = value;
        }

        public override Type ContentType
        {
            get
            {
                if (!_left.HasValue) return null;
                if (_left.Value)
                {
                    var union1 = _t1 as Union;
                    return union1 != null ? union1.ContentType : typeof (T1);
                }

                var union2 = _t2 as Union;
                return union2 != null ? union2.ContentType : typeof(T2);
            }
        }

        public override object Content
        {
            get
            {
                if(!_left.HasValue)
                    throw new NullReferenceException();
                if (_left.Value)
                {
                    var union1 = _t1 as Union;
                    return union1 != null ? union1.Content : _t1;
                }
                var union2 = _t2 as Union;
                return union2 != null ? union2.Content : _t2;
            }
            set
            {
                if (value is T1)
                {
                    _t1 = (T1) value;
                    _t2 = default(T2);
                    _left = true;
                    return;
                }
                if (value is T2)
                {
                    _t2 = (T2) value;
                    _t1 = default(T1);
                    _left = false;
                    return;
                }



                if (Union.IsUnionType(typeof (T1)) &&
                    Union.GetAcceptableTypes(typeof (T1))
                        .Any(p => p.GetTypeInfo().IsAssignableFrom(value.GetType().GetTypeInfo())))
                {
                    var tt = (Union) ((object) Activator.CreateInstance<T1>());
                    tt.Content = value;
                    _t2 = default(T2);
                    _t1 = (T1) ((object) tt);
                    _left = true;
                    return;
                }

                if (Union.IsUnionType(typeof (T2)) &&
                    Union.GetAcceptableTypes(typeof (T2))
                        .Any(p => p.GetTypeInfo().IsAssignableFrom(value.GetType().GetTypeInfo())))
                {

                    var tt = (Union) ((object) Activator.CreateInstance<T2>());
                    tt.Content = value;
                    _t2 = (T2) ((object) tt);
                    _t1 = default(T1);
                    _left = false;
                    return;
                }

                try
                {
                    _t1 = (T1)Convert.ChangeType(value, typeof(T1));
                    _t2 = default(T2);
                    _left = true;
                    return;
                }
                catch
                {

                }

                try
                {
                    _t2 = (T2)Convert.ChangeType(value, typeof(T2));
                    _t1 = default(T1);
                    _left = false;
                    return;
                }
                catch
                {

                }

                if (Union.IsUnionType(typeof(T1)))
                {
                    var tt = (Union)((object)Activator.CreateInstance<T1>());
                    try
                    {
                        tt.Content = value;
                        _t1 = (T1) ((object) tt);
                        _t2 = default(T2);
                        _left = true;
                        return;
                    }
                    catch
                    {
                        
                    }
                }

                if (Union.IsUnionType(typeof(T2)))
                {
                    var tt = (Union)((object)Activator.CreateInstance<T2>());
                    try
                    {
                        tt.Content = value;
                        _t2 = (T2)((object)tt);
                        _t1 = default(T1);
                        _left = false;
                        return;
                    }
                    catch
                    {

                    }
                }


                throw new InvalidCastException($"Cannot set value");
            }
        }

        public static implicit operator T1(Union<T1, T2> obj)
        {
            return (T1) obj.Content;
        }

        public static implicit operator T2(Union<T1, T2> obj)
        {
            return (T2)obj.Content;
        }

        public static implicit operator Union<T1, T2>(T1 obj)
        {
            return new Union<T1, T2>(obj);
        }

        public static implicit operator Union<T1, T2>(T2 obj)
        {
            return new Union<T1, T2>(obj);
        }

    }

    public static class UnionExtensions
    {
        public static bool Is<T>(this Union union)
        {
            return union.Content is T;
        }

        public static T As<T>(this Union union, bool returnDefault = true)
        {
            if (union.Is<T>())
                return (T) union.Content;
            if (returnDefault)
                return default(T);
            throw new InvalidCastException();
        }
    }
}