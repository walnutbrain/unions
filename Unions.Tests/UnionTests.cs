﻿using System;
using NUnit.Framework;

namespace Unions.Tests
{
    [TestFixture]
    public class UnionTests
    {
        [Test]
        public void DontTakeNoUnionTypes()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var types = Union.GetAcceptableTypes(typeof (int));
            });
        }

        [Test]
        public void DontTakeUnionRootType()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var types = Union.GetAcceptableTypes(typeof (Union));
            });
        }

        public class DummyUnknownUnion : Union
        {
            public override Type ContentType
            {
                get { throw new NotImplementedException(); }
            }

            public override object Content
            {
                get { throw new NotImplementedException(); }
                set { throw new NotImplementedException(); }
            }
        }

        [Test]
        public void DontTakeUnknownUnionTyped()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var types = Union.GetAcceptableTypes(typeof (DummyUnknownUnion));
            });
        }

        [Test]
        public void TakeUnionBaseGenericType()
        {
            Assert.DoesNotThrow(() =>
            {
                var types = Union.GetAcceptableTypes(typeof (Union<int, string>));
            }
                );
        }

        [Test]
        public void TakeUnionGenericExtType()
        {
            Assert.DoesNotThrow(() =>
            {
                var types = Union.GetAcceptableTypes(typeof (Union<int, string, float>));
            }
                );
        }

        [Test]
        public void CheckUnionBaseGeneric()
        {
            var types = Union.GetAcceptableTypes(typeof (Union<int, string>));
            Assert.AreEqual(types.Length, 2);
            Assert.AreEqual(types[0], typeof (int));
            Assert.AreEqual(types[1], typeof (string));
        }

        [Test]
        public void CheckUnionBase4Generic()
        {
            var types = Union.GetAcceptableTypes(typeof (Union<int, string, double, bool>));
            Assert.AreEqual(types.Length, 4);
            Assert.AreEqual(types[0], typeof (int));
            Assert.AreEqual(types[1], typeof (string));
            Assert.AreEqual(types[2], typeof (double));
            Assert.AreEqual(types[3], typeof (bool));
        }

        [Test]
        public void CheckUnionBase4GenericNotDistinc()
        {
            var types = Union.GetAcceptableTypes(typeof (Union<int, string, double, int>));
            Assert.AreEqual(types.Length, 3);
            Assert.AreEqual(types[0], typeof (int));
            Assert.AreEqual(types[1], typeof (string));
            Assert.AreEqual(types[2], typeof (double));
        }

        [Test]
        public void ThrowOnEmptyRead()
        {
            var union = new Union<int, string>();
            Assert.Throws<NullReferenceException>(() =>
            {
                var a = union.Content;
            });
        }

        [Test]
        public void ThrowOnDontTypeSet()
        {
            var union = new Union<int, string>();
            Assert.Throws<InvalidCastException>(() =>
            {
                union.Content = 5.8;
            });
        }

        [Test]
        public void SetValue2()
        {
            var union = new Union<int, string>();
            union.Content = 5;
            Assert.AreEqual(5, (int) union.Content);
        }

        [Test]
        public void SetValue3()
        {
            var union = new Union<int, string, double>();
            union.Content = 5.5;
            Assert.AreEqual(5.5, (double) union.Content);
        }

        [Test]
        public void SetValue2ConvertibleType()
        {
            var union = new Union<float, string>();
            union.Content = 5;
            Assert.AreEqual(5, union.Content);
        }

        [Test]
        public void SetValue3ConvertibleType()
        {
            var union = new Union<float, bool, string>();
            union.Content = true;
            Assert.AreEqual(true, union.Content);
        }

        [Test]
        public void ImplicitSet2Type()
        {
            Union<int, string> union = 5;
            
            Assert.AreEqual(5, union.Content);

            union = "abcd";
            Assert.AreEqual("abcd", union.Content);
        }

        [Test]
        public void ImplicitGet2Type()
        {
            Union<int, string> union = 5;

            int a = union;
            Assert.AreEqual(a, 5);

            union = "abcd";
            string b = union;
            Assert.AreEqual("abcd", b);
        }

        [Test]
        public void ThrowImplicitGet2Type()
        {
            Union<int, string> union = 5;
            Assert.Throws<InvalidCastException>(() => { string b = union; });
        }

        [Test]
        public void ImplicitSet3Type()
        {
            Union<int, string, Guid> union = 5;

            Assert.AreEqual(5, union.Content);

            union = "abcd";
            Assert.AreEqual("abcd", union.Content);
        }

        [Test]
        public void ImplicitGet3Type()
        {
            Union<int, string, Guid> union = 5;

            int a = union;
            Assert.AreEqual(a, 5);

            union = "abcd";
            string b = union;
            Assert.AreEqual("abcd", b);
        }


        [Test]
        public void ImplicitSet5Type()
        {
            Union<int, string, Guid, bool, double> union = 5.5;

            Assert.AreEqual(5.5, union.Content);

            union = "abcd";
            Assert.AreEqual("abcd", union.Content);
        }

        [Test]
        public void ImplicitGet5Type()
        {
            Union<int, string, Guid, bool, double> union = true;

            bool a = union;
            Assert.AreEqual(a, true);

            union = "abcd";
            string b = union;
            Assert.AreEqual("abcd", b);
        }

        [Test]
        public void Is3Test()
        {
            Union<int, string, Guid> union = 5;

            Assert.True(union.Is<int>());

            union = "abcd";
            string b = union;
            Assert.False(union.Is<int>());
        }

        [Test]
        public void As3Test()
        {
            Union<int, string, Guid> union = 5;

            Assert.AreEqual(union.As<int>(), 5);

            union = "abcd";
            Assert.AreEqual(union.As<int>(), 0);

            Assert.Throws<InvalidCastException>(() => union.As<int>(false));
        }

    }
}
