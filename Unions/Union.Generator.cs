﻿
namespace Unions
{

	public class Union<T0, T1, T2> : Union<T0, Union<T1, T2>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2>(T0 value)
		{
			return new Union<T0, T1, T2>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2>(T1 value)
		{
			return new Union<T0, T1, T2>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2>(T2 value)
		{
			return new Union<T0, T1, T2>(value);
		}


    }       
	public class Union<T0, T1, T2, T3> : Union<T0, Union<T1, T2, T3>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3>(T0 value)
		{
			return new Union<T0, T1, T2, T3>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3>(T1 value)
		{
			return new Union<T0, T1, T2, T3>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3>(T2 value)
		{
			return new Union<T0, T1, T2, T3>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3>(T3 value)
		{
			return new Union<T0, T1, T2, T3>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4> : Union<T0, Union<T1, T2, T3, T4>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5> : Union<T0, Union<T1, T2, T3, T4, T5>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5, T6> : Union<T0, Union<T1, T2, T3, T4, T5, T6>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public Union(T6 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5, T6> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5, T6> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5, T6> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5, T6> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5, T6> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5, T6> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6>(value);
		}

		public static implicit operator T6(Union<T0, T1, T2, T3, T4, T5, T6> union)
		{
			return (T6) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6>(T6 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5, T6, T7> : Union<T0, Union<T1, T2, T3, T4, T5, T6, T7>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public Union(T6 value) : base(value)
		{
		}

		public Union(T7 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5, T6, T7> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5, T6, T7> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5, T6, T7> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5, T6, T7> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5, T6, T7> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5, T6, T7> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7>(value);
		}

		public static implicit operator T6(Union<T0, T1, T2, T3, T4, T5, T6, T7> union)
		{
			return (T6) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7>(T6 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7>(value);
		}

		public static implicit operator T7(Union<T0, T1, T2, T3, T4, T5, T6, T7> union)
		{
			return (T7) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7>(T7 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5, T6, T7, T8> : Union<T0, Union<T1, T2, T3, T4, T5, T6, T7, T8>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public Union(T6 value) : base(value)
		{
		}

		public Union(T7 value) : base(value)
		{
		}

		public Union(T8 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(value);
		}

		public static implicit operator T6(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8> union)
		{
			return (T6) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(T6 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(value);
		}

		public static implicit operator T7(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8> union)
		{
			return (T7) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(T7 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(value);
		}

		public static implicit operator T8(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8> union)
		{
			return (T8) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(T8 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> : Union<T0, Union<T1, T2, T3, T4, T5, T6, T7, T8, T9>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public Union(T6 value) : base(value)
		{
		}

		public Union(T7 value) : base(value)
		{
		}

		public Union(T8 value) : base(value)
		{
		}

		public Union(T9 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(value);
		}

		public static implicit operator T6(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> union)
		{
			return (T6) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T6 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(value);
		}

		public static implicit operator T7(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> union)
		{
			return (T7) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T7 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(value);
		}

		public static implicit operator T8(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> union)
		{
			return (T8) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T8 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(value);
		}

		public static implicit operator T9(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> union)
		{
			return (T9) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T9 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> : Union<T0, Union<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public Union(T6 value) : base(value)
		{
		}

		public Union(T7 value) : base(value)
		{
		}

		public Union(T8 value) : base(value)
		{
		}

		public Union(T9 value) : base(value)
		{
		}

		public Union(T10 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}

		public static implicit operator T6(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T6) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T6 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}

		public static implicit operator T7(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T7) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T7 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}

		public static implicit operator T8(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T8) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T8 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}

		public static implicit operator T9(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T9) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T9 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}

		public static implicit operator T10(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> union)
		{
			return (T10) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T10 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> : Union<T0, Union<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public Union(T6 value) : base(value)
		{
		}

		public Union(T7 value) : base(value)
		{
		}

		public Union(T8 value) : base(value)
		{
		}

		public Union(T9 value) : base(value)
		{
		}

		public Union(T10 value) : base(value)
		{
		}

		public Union(T11 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T6(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T6) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T6 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T7(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T7) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T7 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T8(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T8) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T8 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T9(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T9) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T9 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T10(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T10) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T10 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}

		public static implicit operator T11(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> union)
		{
			return (T11) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T11 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> : Union<T0, Union<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public Union(T6 value) : base(value)
		{
		}

		public Union(T7 value) : base(value)
		{
		}

		public Union(T8 value) : base(value)
		{
		}

		public Union(T9 value) : base(value)
		{
		}

		public Union(T10 value) : base(value)
		{
		}

		public Union(T11 value) : base(value)
		{
		}

		public Union(T12 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T6(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T6) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T6 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T7(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T7) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T7 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T8(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T8) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T8 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T9(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T9) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T9 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T10(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T10) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T10 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T11(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T11) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T11 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}

		public static implicit operator T12(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> union)
		{
			return (T12) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T12 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> : Union<T0, Union<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public Union(T6 value) : base(value)
		{
		}

		public Union(T7 value) : base(value)
		{
		}

		public Union(T8 value) : base(value)
		{
		}

		public Union(T9 value) : base(value)
		{
		}

		public Union(T10 value) : base(value)
		{
		}

		public Union(T11 value) : base(value)
		{
		}

		public Union(T12 value) : base(value)
		{
		}

		public Union(T13 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T6(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T6) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T6 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T7(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T7) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T7 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T8(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T8) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T8 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T9(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T9) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T9 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T10(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T10) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T10 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T11(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T11) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T11 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T12(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T12) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T12 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}

		public static implicit operator T13(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> union)
		{
			return (T13) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T13 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> : Union<T0, Union<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public Union(T6 value) : base(value)
		{
		}

		public Union(T7 value) : base(value)
		{
		}

		public Union(T8 value) : base(value)
		{
		}

		public Union(T9 value) : base(value)
		{
		}

		public Union(T10 value) : base(value)
		{
		}

		public Union(T11 value) : base(value)
		{
		}

		public Union(T12 value) : base(value)
		{
		}

		public Union(T13 value) : base(value)
		{
		}

		public Union(T14 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T6(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T6) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T6 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T7(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T7) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T7 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T8(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T8) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T8 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T9(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T9) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T9 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T10(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T10) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T10 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T11(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T11) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T11 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T12(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T12) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T12 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T13(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T13) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T13 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}

		public static implicit operator T14(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> union)
		{
			return (T14) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T14 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(value);
		}


    }       
	public class Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> : Union<T0, Union<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>>
	{
		public Union() : base()
		{
		}

		public Union(T0 value) : base(value)
		{
		}

		public Union(T1 value) : base(value)
		{
		}

		public Union(T2 value) : base(value)
		{
		}

		public Union(T3 value) : base(value)
		{
		}

		public Union(T4 value) : base(value)
		{
		}

		public Union(T5 value) : base(value)
		{
		}

		public Union(T6 value) : base(value)
		{
		}

		public Union(T7 value) : base(value)
		{
		}

		public Union(T8 value) : base(value)
		{
		}

		public Union(T9 value) : base(value)
		{
		}

		public Union(T10 value) : base(value)
		{
		}

		public Union(T11 value) : base(value)
		{
		}

		public Union(T12 value) : base(value)
		{
		}

		public Union(T13 value) : base(value)
		{
		}

		public Union(T14 value) : base(value)
		{
		}

		public Union(T15 value) : base(value)
		{
		}

		public static implicit operator T0(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T0) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T0 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T1(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T1) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T1 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T2(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T2) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T2 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T3(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T3) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T3 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T4(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T4) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T4 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T5(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T5) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T5 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T6(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T6) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T6 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T7(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T7) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T7 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T8(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T8) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T8 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T9(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T9) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T9 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T10(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T10) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T10 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T11(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T11) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T11 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T12(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T12) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T12 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T13(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T13) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T13 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T14(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T14) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T14 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}

		public static implicit operator T15(Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> union)
		{
			return (T15) union.Content;
		}

		public static implicit operator Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T15 value)
		{
			return new Union<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(value);
		}


    }       
}

